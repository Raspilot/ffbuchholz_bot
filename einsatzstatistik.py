import requests
from bs4 import BeautifulSoup as bs
import time
import datetime
import json

url = 'http://ffbuchholz.de/einsatzticker'

tableClasses = ['post-entry',
                'post-entry-type-page',
                'post-entry-734']
tableIdents = {'class': ['posts-data-table',
                         'dataTable',
                         'no-footer',
                         'dtr-inline'],
               'id': ['posts-table-1']
               }
year = 2019
einsätze = None
data = {'Titel': [],
        'Kategorien': [],
        'Datum': [],
        'Stichwort': [],
        'Ortsteil': []}
month = ["Jan.",
         "Feb.",
         "März",
         "Apr.",
         "Mai",
         "Jun.",
         "Juli",
         "Aug.",
         "Sept.",
         "Okt.",
         "Nov.",
         "Dez."]


def concat(lists=[]):
    output = ''
    for a in lists:
        output = output + a
    return (output)


def split(text, regex=[], include_regex=False):
    output = [text]
    for a in regex:
        if output == [text]:
            output = []
            for b in range(len(text.split(a))):
                output.append(text.split(a)[b])
                if include_regex and b < len(text.split(a)) - 1:
                    output.append(a)
        elif a != regex[0]:
            b = 0
            while b < len(output):
                text = output[b]
                del output[b]
                for c in split(text, [a], include_regex=include_regex):
                    output.insert(b, c)
                    b += 1
    return (output)


def probabilityPerDay(HPD=24,
                      precision=1,
                      abrunden=10):
    messabschnitte = HPD * precision
    zeitraeume = []
    for a in range(int(messabschnitte)):
        start = (a / precision)
        ende = (start + 1 / messabschnitte * HPD)
        text_start = (str(int(start % HPD)) + ":" + str(round((start % 1) * 60))).replace(":0", ":00")
        text_ende = (str(int(ende % HPD)) + ":" + str(round((ende % 1) * 60))).replace(":0", ":00")
        zeitraum = text_start + " - " + text_ende + " Uhr"
        anzahl = len([em for em in einsätze
                      if start <= (time.gmtime(em['Datum']).tm_hour
                                   + time.gmtime(em['Datum']).tm_min
                                   / 60.0)
                      < ende])
        wahrscheinlichkeit = anzahl / time.gmtime(time.time()).tm_yday * 100
        zeitraeume.append(wahrscheinlichkeit)
    return (zeitraeume)


def getData(url=url):
    try:
        print("Updating values...")
        r = requests.get(url)
        print("recieved values from website.")
        doc = bs(r.text, 'html.parser')
        print("Updated values.")
        try:
            with open("website.html", "w") as file:
                file.write(str(r.text.encode('utf-8-sig')))
                file.close()
                print("Saved document.")
        except:
            print("Daten konnten nicht gespeichert werden.")
            pass
    except Exception as e:
        print(e)
        print('Aktuelle Daten von ' + str(
            url) + ' konnten nicht abgerufen werden.\nArbeit wird mit alten Daten fortgesetzt.')
        pass
        print("Read document...")
        doc = bs(open("website.html", "r").read(), 'html.parser')
        print("Read document.")
    return (doc)


class einsatz:
    def __init__(self, title, category, timestamp, keyword, location):
        self.title = title
        self.category = category
        self.timestamp = timestamp
        self.keyword = keyword
        self.location = location

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def __repr__(self):
        return (dict(self))


if __name__ == "__main__":
    doc = getData()
    tableIndex = None
    einsätze = []

    for x in range(len(doc.find_all())):
        try:
            matches = (set(doc.find_all()[x].attrs['class']) &
                       set(tableIdents['class']))
        except KeyError:
            pass
        if bool(matches):
            tableIndex = x
    if tableIndex is not None:
        table = doc.find_all()[tableIndex]
        year = int([a.text for a in doc.find_all('span') if "Einsatzticker " in a.text][0].split(" ")[-1])
        # <span class="avia_iconbox_title">Einsätze in 2019:</span>
        # einsätze = [{'Titel': x.find_all('td')[0].text.strip(),
        #              'Kategorien': x.find_all('td')[1].text.split(',')[2].strip(),
        #              'Datum': time.mktime(datetime.datetime.strptime(x.find_all('td')[3].text.strip(),
        #                                                              "%Y-%m-%d %H:%M:%S").timetuple()) + 3600,
        #              'Stichwort': concat(split(x.find_all('td')[0].text.strip(), [' in ', ' auf '], True)[0:-2]),
        #              'Ortsteil': split(x.find_all('td')[0].text.strip(), [' in ', ' auf '])[-1]}
        #             for x in table.find_all('tbody')[0].find_all('tr')]
        tr_list = table.find_all('tbody')[0].find_all('tr')
        for eintrag in tr_list:
            einsätze.append(einsatz(title=x.find_all('td')[0].text.strip(),
                                    category=x.find_all('td')[1].text.split(',')[2].strip(),
                                    timestamp=time.mktime(datetime.datetime.strptime(x.find_all('td')[3].text.strip(),
                                                                                     "%Y-%m-%d %H:%M:%S").timetuple()) + 3600,
                                    keyword=concat(split(x.find_all('td')[0].text.strip(), [' in ', ' auf '], True)[0:-2]),
                                    location=split(x.find_all('td')[0].text.strip(), [' in ', ' auf '])[-1]))
        with open(f'einsätze{time.gmtime(time.time()).tm_year}.txt', 'w') as file:
            file.write(json.dumps(einsätze))
    else:
        print('Couldn\'t find your object')
    try:
        with open(f'einsätze{time.gmtime(time.time()).tm_year}.txt', 'r') as file:
            einsätze = eval(file.read())
    except:
        einsätze = []
        print('No emergencies saved.')

    for a in einsätze:
        for b in data:
            if not a[b] in data[b]:
                data[b].append(a[b])

    print("\n\n")
    banner = "="
    label = "Freiwillige Feuerwehr Buchholz " + str(year)
    side = 9
    print(banner * (len(label) + 2 * (side + 2)))
    print(banner * side + "  " + label + "  " + side * banner)
    print(banner * (len(label) + 2 * (side + 2)))
    print("\n" + banner * int(side / 2) + "> " + "Einsatzstatistik:\n")

    # for a in data:
    #    print(a + ':')
    #    for b in data[a]:
    #        print('-' + a + '-->   \t' + str(b))
    #    print('Anzahl ' + a + ': ' + str(len(data[a])))

    """
    print("Relative Wahrscheinlichkeit, dass ein Einsatz im Zeitraum liegt:")
    HPD = 24
    GENAUIGKEIT = 2
    messabschnitte = HPD * GENAUIGKEIT
    zeitraeume = []
    abrunden = 1
    for a in range(int(messabschnitte)):
        start = (a/GENAUIGKEIT)
        ende = (start+1/messabschnitte * HPD)
        text_start = (str(int(start%HPD)) + ":" + str(round((start%1)*60)))
        text_ende = (str(int(ende%HPD)) + ":" + str(round((ende%1)*60)))
        zeitraum = text_start + " - " + text_ende + " Uhr"
        zeitraum = zeitraum.replace(":0 ", ":00 ")
        anzahl = len([em for em in einsätze
                      if start <= (time.gmtime(em['Datum']).tm_hour
                               + time.gmtime(em['Datum']).tm_min
                               / 60.0)
                      < ende])
        wahrscheinlichkeit = anzahl / len(einsätze) * 100
        zeitraeume.append(wahrscheinlichkeit)
        print(zeitraum + "  \t" + str(round(wahrscheinlichkeit*pow(10,abrunden))/pow(10,abrunden)) + "% (" + str(anzahl) + ")")
    print(str(round(sum(zeitraeume))) + "% der Einsätze berücksichtigt.")

    print("Absolute Wahrscheinlichkeit, dass ein Einsatz im Zeitraum liegt:")
    HPD = 24
    GENAUIGKEIT = 1
    messabschnitte = HPD * GENAUIGKEIT
    zeitraeume = []
    abrunden = 10
    for a in range(int(messabschnitte)):
        start = (a/GENAUIGKEIT)
        ende = (start+1/messabschnitte * HPD)
        text_start = (str(int(start%HPD)) + ":" + str(round((start%1)*60))).replace(":0",":00")
        text_ende = (str(int(ende%HPD)) + ":" + str(round((ende%1)*60))).replace(":0",":00")
        zeitraum = text_start + " - " + text_ende + " Uhr"
        anzahl = len([em for em in einsätze
                      if start <= (time.gmtime(em['Datum']).tm_hour
                               + time.gmtime(em['Datum']).tm_min
                               / 60.0)
                      < ende])
        wahrscheinlichkeit = anzahl / time.gmtime(time.time()).tm_yday * 100
        zeitraeume.append(wahrscheinlichkeit)
        print(zeitraum + "  \t" + str(round(wahrscheinlichkeit*pow(10,abrunden))/pow(10,abrunden)) + "% (" + str(anzahl) + ")")
    print("Wahrscheinlichkeit für einen Einsatz innerhalb der nächsten 24 Stunden: " + str(round(sum(zeitraeume))) + "%.")
    """
    precision = 1
    HPD = 24
    abrunden = 2
    for a in range(len(probabilityPerDay(precision=precision))):
        start = (a / precision)
        ende = (start + 1 / (HPD * precision) * HPD)
        text_start = (str(int(start % HPD)) + ":" + str(round((start % 1) * 60))).replace(":0", ":00")
        text_ende = (str(int(ende % HPD)) + ":" + str(round((ende % 1) * 60))).replace(":0", ":00")
        zeitraum = text_start + " <= x < " + text_ende + " Uhr"
        anzahl = len([em for em in einsätze
                      if start <= (time.gmtime(em['Datum']).tm_hour
                                   + time.gmtime(em['Datum']).tm_min
                                   / 60.0)
                      < ende])
        wahrscheinlichkeit = anzahl / time.gmtime(time.time()).tm_yday * 100
        print(
            zeitraum + "     \t" + str(round(wahrscheinlichkeit * pow(10, abrunden)) / pow(10, abrunden)) + "% (" + str(
                anzahl) + ")")

    MPY = len(month)
    print("Einsätze in {} (Gesamt: {} Einsätze an {} Tagen):".format(year, len(einsätze),
                                                                     time.gmtime(time.time()).tm_yday))
    for a in range(MPY):
        print("->" * ((a + 1) % MPY == time.gmtime(time.time()).tm_mon) + str(month[a % MPY]) + "\t " + str(len(
            [em for em in einsätze if
             time.gmtime(em['Datum']).tm_mon == a + 1 and time.gmtime(em['Datum']).tm_year == year])))
