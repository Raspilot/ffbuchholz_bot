def replace(text = 'Hello World', originals = ['Hello', 'World'], new = ['Hi', 'there']):
    if len(originals) == len(new):
        for index in range(len(originals)):
            text = text.replace(str(originals[index]), str(new[index]))
        return(text)
    else:
        print('Arrays not of same length.')
        raise(Exception)
def remove(text, originals):
    return(replace(text, originals, ['' for x in range(len(originals))]))

def split(text, regex = []):
    output = ['']
    for x in text:
        if not x in regex:
            output[-1] = output[-1] + x
        elif output[-1] != '':
            output.append('')
    return(output)
def replaceInd(text, index, char = ''):
    output = ''
    a = 0
    #print(text)
    #print(index)
    #print(char)
    while a < len(text):
        #print(a)
        #print(output)
        if a == index or a-len(text) == index:
            #print('a == index')
            output = output + char
            a+=1
        else:
            #print('a != index')
            output = output + text[a]
            a+=1
    return(output)
def replaceInds(text, index, char = ''):
    if char == '':
        output = text
        for a in range(len(index)):
            output = replaceInd(output, index[0], '')
        return(output)
    if len(index) == len(char):
        output = text
        for x in range(len(index)):
            output = replaceInd(output, index[x], char[x])
        return(output)
    else:
        print('Arrays not of same length.')
        raise(Exception)

def sDiv(a, b):
    try:
        return(eval(str(a))/eval(str(b)))
    except ZeroDivisionError:
        return(0)
    
def getRng(text, rng, op = False):
    output = ''
    for a in range(len(text)):
        if op:
            if a in rng:
                output = output + text[a]
        else:
            if not (a in rng):
                output = output + text[a]
    return(output + ')')

def find(text, char):
    output = []
    for a in range(len(text)):
        if text[a] in char:
            output.append(a)
    return(output)

def extract(text):
    output = ''
    counter = 0
    for a in range(len(text)):
        output = output + text[a]
        if text[a] == '(':
            counter += 1
        if text[a] == ')':
            counter -= 1
        if counter == 0:
            return(output)
            break
    raise(SyntaxError())

def checkLayer(text, index = None, brackets = '()'): # Check, how deep the index is in the brackets.
    if len(find(text, brackets[0])) != len(find(text, brackets[1])):
        raise(SyntaxError(brackets[0] + ' and ' + brackets[1] + ' not of same amount: ' + text + '\n' +
                          str(len(find(text, brackets[0]))) + '\t' + str(len(find(text, brackets[1])))))
    counter = 0
    array = []
    for a in range(len(text)):
        # go deeper
        if text[a] == brackets[0]:
            counter += 1
            array.append(counter)
        # raise back up
        elif text[a] == brackets[1]:
            array.append(counter)
            counter -= 1
        else:
            array.append(counter)
    if index == None:
        return(array)
    else:
        return(array[index])

def crop(text, regex = ' '):
    # removing from front
    while text[0] in regex:
        text = replaceInd(text, 0, '')
    # removing from back
    while text[-1] in regex:
        text = replaceInd(text, -1, '')
    return(text)

def insert(text, add, index):
    output = ''
    for a in range(len(text)):
        if a != index:
            output = output + text[a]
        else:
            output = output + add + text[a]
    return(output)

def checkBrackets(text, brackets = '()'):
    # check if there is an equal amount of the first and the opposing bracket
    output = []
    for a in brackets:
        output.append(len([b for b in text if b == a]))
    return(output)

def cropBrackets(text, index = 0, brackets = '()'):
    output = ''
    area = getBracketRng(text, index, brackets)
    for a in range(area[0]+1, area[1]):
        output = output + text[a]
    return(output)

def getBracketRng(text, index = None, brackets = '()'):
    if index == None:
        output = []
        for a in range(len(text)):
            try:
                output.append([[b for b in find(text, brackets[0]) if b <= a and checkLayer(text, a) == checkLayer(text, b)][-1],
                               [b for b in find(text, brackets[1]) if a <= b and checkLayer(text, a) == checkLayer(text, b)][0]])
            except IndexError:
                output.append([0,len(text)])
        return(output)
    else:
        while index < 0:
            index += len(text)
        if checkLayer(text, index) != 0:
            return([[b for b in find(text, brackets[0]) if b <= index and checkLayer(text, index) == checkLayer(text, b)][-1],
                    [b for b in find(text, brackets[1]) if index <= b and checkLayer(text, index) == checkLayer(text, b)][0]])
        else:
            return([0, len(text)])
if __name__=='__main__':
    myVal = '((()()) )'
    print(str(getBracketRng(myVal, 5)) + '\n')
    print(str(getBracketRng(myVal)) + '\n')
    print(cropBrackets(myVal, 8))