import json
import logging
import threading
from functools import wraps

from telegram import ChatAction, ParseMode
from telegram.error import NetworkError
from telegram.ext import Updater

path_to_sensible_data = "../ffbuchholz_Data/"
path_to_user_data = path_to_sensible_data + "userdata/"

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# indirect functions
def read_file(filename, path=path_to_sensible_data):
    try:
        return json.loads(open(path + filename, "r", encoding="utf-8").read())
    except FileNotFoundError as e:
        if filename.endswith(".json"):
            return dict()
        elif filename.endswith(".txt"):
            return ""
        else:
            raise e


def write_file(filename, content, path=path_to_sensible_data):
    assert filename, "No filename given"
    assert "." in filename, "Missing \".\" in filename"
    assert not filename.endswith("."), f"{filename} cannot end with \".\""
    if filename.endswith(".json"):
        assert type(content) == dict, "Content of .json file cannot be other than dict"
    if filename.endswith(".txt"):
        assert type(content) == str, "Content of .txt file cannot be other than string"
    if type(content) == dict:
        with open(path + filename, "w", encoding="utf-8") as file:
            file.write(json.dumps(content))
            file.close()
    elif type(content) == str:
        with open(path + filename, "w", encoding="utf-8") as file:
            file.write(content)
            file.close()
    return True


def get_command_description(command, detail="short"):
    try:
        command_description = read_file("command_description.json")
        try:
            try:
                command = command_description[command]
                try:
                    return command[detail]
                except KeyError:
                    print(f"{detail} description for {command} does not exist.")
                    return
            except KeyError:
                print(f"{command} does not exist.")
                return
        except KeyError:
            return
    except FileNotFoundError:
        return


def get_support(user_id=None):
    data = read_file("general_information.json")
    support_info = data["support"]
    least_clients = min([len(support_info[a]["clients"]) for a in support_info])
    supporter = ""
    client = user_id
    if str(client) in support_info:
        return client
    else:
        for sp in support_info:
            if client in support_info[sp]["clients"]:
                supporter = sp
                break
        if not supporter:
            supporter = [a for a in support_info if len(support_info[a]["clients"]) <= least_clients][0]
    data["support"][str(supporter)]["clients"].append(client)
    write_file("general_information.json", data)
    return int(supporter)


def is_support(user_id):
    return user_id == get_support(user_id)


def stop(process):
    process.stop()
    process.is_idle = False


# decorators
def send_typing_action(func):
    """Sends typing action while processing func command."""

    @wraps(func)
    def command_func(update, context, *args, **kwargs):
        context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
        return func(update, context, *args, **kwargs)

    return command_func


def support_only(func):
    """Allow access to this function for support only"""

    @wraps(func)
    def command_func(update, context, *args, **kwargs):
        if is_support(update.message.chat_id):
            print(f"Granted access to {func} for {update.message.from_user}")
            return func(update, context, *args, **kwargs)
        else:
            print(f"Denied access to {func} for {update.message.from_user}")

    return command_func


# user commands
@send_typing_action
def start(update, context):
    context.bot.send_message(chat_id=update.message.chat_id,
                             text=get_command_description("start", detail="long"),
                             parse_mode=ParseMode.HTML)


@send_typing_action
def help(update, context):
    parameters = update.message.text.split(" ")[1:]
    if not parameters:
        message = "Die folgenden Befehle stehen zur Verfügung:\n"
        for a in updater.dispatcher.handlers[0]:
            if type(a) == CommandHandler:
                command = a.command[0]
                command_description = get_command_description(command)
                if command_description:
                    message = message + "\n/" + a.command[0] + " - " + command_description
                else:
                    support_list = read_file("general_information.json")["support"]
                    if is_support(update.message.chat_id):
                        message = message + "\n/" + a.command[0]
                    print("Es gibt (noch) keine Erklärung für \"" + a.command[0] + "\"")
    else:
        message = "Hier sind Details zu den gefragten Befehlen:\n"
        for command in parameters:
            list_of_command_handlers = [a.command[0] for a in updater.dispatcher.handlers[0] if
                                        type(a) == CommandHandler]
            if command in list_of_command_handlers:
                command_description = get_command_description(command)
                if command_description or is_support(update.message.chat_id):
                    message = message + "\n/" + command + " " + command_description
    context.bot.send_message(chat_id=update.message.chat_id, text=message, parse_mode=ParseMode.HTML)


@send_typing_action
def support(update, context):
    parameters = update.message.text.split(" ")[1:]
    if parameters and not update.message.from_user["is_bot"]:
        supporter = get_support(update.message.from_user['id'])
        user = str(update.message.from_user)
        problem = " ".join(parameters)
        context.bot.forward_message(chat_id=supporter, from_chat_id=update.message.chat_id,
                                    message_id=update.message.message_id,
                                    text=user + " hat folgendes Problem mit dem Vertretungsplan_Bot:\n\n" + problem)
        context.bot.send_message(chat_id=update.message.from_user["id"],
                                 text="Dein Anliegen wurde an deinen Supporter gesendet. Bitte nimm zur Kenntnis, dass dieses Projekt freiwillig betrieben wird und wir manchmal auch keine Zeit haben, um direkt zu antworten.")


@support_only
def set_command_description(update, context):
    print("Hello World!")


def error(update, context):
    context_error_text = ""
    if isinstance(context.error, NetworkError):
        print("NetworkError -> restarting...")
        threading.Thread(target=stop, args=(updater,)).start()
        return
    try:
        """Log Errors caused by Updates."""
        logger.warning('Update "%s" caused error "%s"', update, context.error)
        context.bot.send_message(chat_id=update.message.chat_id,
                                 text="Es ist ein Fehler aufgetreten. Bitte versuche es erneut oder kontaktiere den Support mit /support.")
    except Exception as e:
        print(e)
    supporter = getSupport()
    error_message = f"Error:\n{update.message.from_user['id']}:\" {update.message.text}\" caused:\n\"{context.error}\""
    context.bot.send_message(chat_id=supporter, text=error_message, disable_notification=True)


if __name__ == "__main__":
    updater = Updater(token=read_file("general_information.json")["token"], use_context=True)
    from telegram.ext import CommandHandler

    updater.dispatcher.add_handler(CommandHandler('help', help))
    updater.dispatcher.add_handler(CommandHandler('start', start))

    updater.start_polling()
    print("Program is running...")
    updater.idle()
    print("Program has stopped.")
